<?php
if ( ! defined( 'ABSPATH' ) ) {
    // exit; // Exit if accessed directly
}
/**
* Options Management Administration Screen.\
*
* If accessed directly in a browser this page shows a list of all saved options
* along with editable fields for their values. Serialized data is not supported
* and there is no way to remove options via this page. It is not linked to from
* anywhere else in the admin.
*
* This file is also the target of the forms in core and custom options pages
* that use the Settings API. In this case it saves the new option values
* and returns the user to their page of origin.
*
* @package WordPress
* @subpackage Administration
*/


define('URM_OPTION_COLUMN', 'config_urm_meli');

class DB_URM_Manager
{
  public function __construct() {
    if(!DB_URM_Manager::urmGetOption()) {
      $configOptions = [ 'urm-time' => 30 , 'urm-read' => false ];
      add_option(URM_OPTION_COLUMN,serialize($configOptions));
    }
    $this->option = DB_URM_Manager::urmGetOption();
  }
  /*
  * Get a COLUMN from wp_option table from Db
  */
  public function urmGetOption() {
    $option = get_option(URM_OPTION_COLUMN);
    if(!$option) return false;

    return (array)unserialize($option);
  }

  public function urmGetSettingsOptionsForm()
  {
    if(isset($_POST['urm-time'])) $this->config_urm_meli['urm-time'] = (int)$_POST['urm-time'];
  }

  public function urmUpdateOption($urlPage, $index, $value)
  {
    global $is_IIS;

    $config_urm_meliOption = $this->urmGetOption();

      if( !$this->option['urm-read'] ) $config_urm_meliOption['urm-read'] = true;
      $config_urm_meliOption[$index] = $value;

      $option = serialize($config_urm_meliOption);


    $updateOption = update_option(URM_OPTION_COLUMN,$option);

    if($updateOption) {
    $status = 100;

    $location = $urlPage.'&settings-updated' ;
    wp_redirect($location);
  } else {
    $location = $urlPage ;
    wp_redirect($location);
  }
  }
}

?>
