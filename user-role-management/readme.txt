User Role Management
  Thank you for installing and using User Role Management.
  This plugin allows you to manage allowed content for users, so as to access only allowed content.

  User Role Management was developed to be used in virtual products, linking a created post referring to the content of the product sold.

  How to use this plugin?

  When installed, the plugin automatically creates all user roles by reference to the Woocommerce product id
  (Ex: subscriber-4032, where 4032 is the id of a virtual product and the "subscriber" is the characteristic of the user).

  From this moment on, the plugin is waiting for the default time that users will have to access the purchased content. 
  This setting occurs after reading this guide.

  Once completed, the plugin will be ready to take over.
  It checks for orders where the access limit has  been exceeded
  (takes into account the date the order was paid) and removes user access (roles).

  Also checks for orders where the access limit has not been exceeded.
  If so, verify that the user corresponding to the purchase has the appropriate permissions (roles).
  If so, the request is ignored. If not, the plugin adds as permissions (roles).

  Suggestions

Like you know, this plugin make exclusive the posts to users that bougth it (subscriber-**** roles).
If you have more than 1 virtual product in more than 1 post, all users that have a subscriber role will can see the private posts.

If you wish to give permission only to users who purchase their product so that other users cannot access the private posts, we recommend installing the Members
plugin which enables the desired configuration. Just indicate who can access private posts (the roles) while they are being created.

What is next?
You will now set the default time  (in days) that users will have access to purchased content.
