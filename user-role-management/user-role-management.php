<?php
/**
* @package User Role Management
* @version 0.0.1
*/
/*
Plugin Name: User Role Management
Plugin URI:
Description: Este plugin cria roles para os usuários de acordo com os produtos virtuais existentes na loja (woocommerce). Ao obter uma venda, o plugin verifica qual ou quais, o(s) produto(s) comprado(s) e adiciona o novo role correspondente ao produto para o usuário, removendo o role padrão (setado em configurações->geral). Após x dias/mês, tempo correspondente ao período em que o conteudo virtual poderá ser acessado, o role correspondente ao produto é removido, adicionando novamente o role padrão.
Author: Luigi Muzy & Yuri Sá
Version: 0.0.1
Author URI:
*/

if (!defined('ABSPATH')) {
    exit;
}

define('URM_PLUGIN_PATH', plugin_dir_path(__FILE__));

require(URM_PLUGIN_PATH.'db.php');
require(URM_PLUGIN_PATH.'template.php');
require(URM_PLUGIN_PATH.'app.php');

// Launch plugin
URM_App::instance();
